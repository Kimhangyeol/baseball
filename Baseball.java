import java.util.*;
//소스 수정용 주석입니다.
class Baseball {
	private static int rnum1, rnum2, rnum3;
	private static int gnum1, gnum2, gnum3;
	private static int strike, ball;
	private static int count = 0;
	private static int tcount = 0;

public static void main(String args[]) {
	System.out.println("2013.11.06");
	create_random_numbers();
	compare_numbers();

}
public static void create_random_numbers() {
	rnum1 = (int)(Math.random() * 10);
	do {
		rnum2 = (int)(Math.random() * 10);
		}while(rnum2 == rnum1);
	do {
		rnum3 = (int)(Math.random() * 10); 	
		}while(rnum3 == rnum1 || rnum3 == rnum2);
	}
public static void compare_numbers() {
	Scanner sc = new Scanner(System.in);
	do{
		strike = ball = 0;
		count++;
		tcount++;
		System.out.println("정답은 " + rnum1 + "" + rnum2 + "" + rnum3 + " 입니다.");
		System.out.println(tcount+"번째 시도입니다.");
		System.out.print("숫자 입력 : ");
		gnum1 = sc.nextInt();
		gnum2 = sc.nextInt();
		gnum3 = sc.nextInt();
		
	if(gnum1 == rnum1) strike++;
	else if(gnum1 == rnum2 || gnum1 == rnum3) ball++;
	if(gnum2 == rnum2) strike++;
	else if(gnum2 == rnum1 || gnum2 == rnum3) ball++;
	if(gnum3 == rnum3) strike++;
	else if(gnum3 == rnum1 || gnum3 == rnum2) ball++;
	System.out.println(strike + "Strike " + ball + "Ball");
	}while(strike < 3);
	
	System.out.println(count + "번만에 정답");
	}
}
